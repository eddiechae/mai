//
//  ChatTableViewController.swift
//  Mai
//
//  Created by Eddie Chae on 29/07/17.
//  Copyright © 2017 Mai. All rights reserved.
//

import UIKit
import FTIndicator
import UserNotifications

enum MaiScenario {
    case intro
    
    //
    
    case tongariro
    case tsunami
    case notification
}

struct Colour {
    static let mainColour = UIColor.init(red: 40/255, green: 40/255, blue: 40/255, alpha: 1)
    static let lightMainColour = UIColor.init(red: 60/255, green: 60/255, blue: 60/255, alpha: 1)
}

struct Font {
    static let lightBodyText = UIFont(name: "HelveticaNeue-Light", size: 18)
    static let mediumBodyText = UIFont(name: "HelveticaNeue-Medium", size: 18)
    static let boldBodyText = UIFont(name: "HelveticaNeue-Bold", size: 18)

}

class ChatTableViewController: FTChatMessageTableViewController,FTChatMessageAccessoryViewDelegate,FTChatMessageAccessoryViewDataSource,FTChatMessageRecorderViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
//    var maiScenario: MaiScenario = .tongariro
//    var maiScenario: MaiScenario = .tsunami
    var maiScenario: MaiScenario = .notification
    
    let sender1 = FTChatMessageUserModel.init(id: "1", name: "Mai", icon_url: "https://yt3.ggpht.com/-gBRgftGI76s/AAAAAAAAAAI/AAAAAAAAAAA/ZMaUQ1Qn3rY/s100-c-k-no-mo-rj-c0xffffff/photo.jpg", extra_data: nil, isSelf: false)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.setLeftBarButton(UIBarButtonItem.init(barButtonSystemItem: UIBarButtonSystemItem.bookmarks, target: self, action: #selector(self.navigateToTripPlan)), animated: true)
        
        // Skin Nav bar
        navigationController?.navigationBar.setBackgroundImage(#imageLiteral(resourceName: "ChatBackground"), for: .default)
        navigationController?.navigationBar.tintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        UINavigationBar.appearance().isTranslucent = false
        
        UINavigationBar.appearance().titleTextAttributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 16)]
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        messageRecordView.recorderDelegate = self
        messageAccessoryView.setupWithDataSource(self , accessoryViewDelegate : self)
        
        addDismissKeyboardTapGestureRecogniser()
        loadDefaultMessages()
        
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToMap(_:)), name: Notification.Name("map"), object: nil)
    }
    
    func navigateToMap(_ notification: Notification) {
        self.performSegue(withIdentifier: "Map", sender: self)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    func loadDefaultMessages() {
        switch maiScenario {
        case .intro:
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.makeMaiMessage("Welcome to Aotearoa Eddie!")
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                    self.makeMaiMessage("Hope you’re looking forward to the adventure ahead.\nTake another look at the tour schedule again. Click on the ⓘ to find out more about each activity.")
                }
            }
            
            break
        case .tongariro:
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
//                self.makeMaiMessage("\n")
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                     self.makeMaiMessage("Hey Eddie, Welcome to Tongariro crossing! \n ")
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                        self.makeMaiMessageWithImage("http://www.greatlaketaupo.com/media/585589/Tongariro-Alpine-Crossing-6.CE9-Vg.jpg")
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
                            self.makeMaiMessage("You are about to enter the litter free zone. Check the map for rubbish bins in your zone. \n ")
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                                self.makeMaiMessageWithImage("http://i.imgur.com/8QPj3tB.png")

                                DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                                    self.makeMaiMessage("Remember the Tongariro Crossing is a litter free zone meaning that you can be fined $400 for littering.\n ")
                                    
                                }
                            }
                        }
                    }
                }
            }
            
            break
        case .tsunami:
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.makeMaiMessage("⚠ WARNING ⚠")
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                    self.makeMaiMessage("Eddie, a tsunami warning has been announced 2 minutes ago. Let's evacuate to the closest safe zone is located HERE:\n")
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                        self.makeMaiMessageWithImage("http://i.imgur.com/CcNmKjA.png")
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                            self.makeMaiMessage("Follow the route to safety.\nCivil Defense have been notified, and will be arriving soon!\n")
                        }
                    }
                }
            }
            break
        case .notification:
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.makeMaiMessage("Welcome to Lake Wakatipu Eddie.")
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                    self.makeMaiMessageWithImage("https://www.transfercar.co.nz/blog/wp-content/uploads/BoatingQtown.jpg")
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                        self.makeMaiMessage("Would you like to hear something really interesting about the lake?\n")
                    }
                }
            }
            
            break
        }
    }
    
    func getAccessoryItemTitleArray() -> [String] {
        return ["Alarm","Camera","Contacts","Mail","Messages","Music","Phone","Photos","Settings","VideoChat","Videos","Weather"]
    }

    
    //MARK: - FTChatMessageAccessoryViewDataSource
    
    func ftChatMessageAccessoryViewModelArray() -> [FTChatMessageAccessoryViewModel] {
        var array : [FTChatMessageAccessoryViewModel] = []
        let titleArray = self.getAccessoryItemTitleArray()
        for i in 0...titleArray.count-1 {
            let string = titleArray[i]
            array.append(FTChatMessageAccessoryViewModel.init(title: string, iconImage: UIImage(named: string)!))
        }
        return array
    }

    //MARK: - FTChatMessageAccessoryViewDelegate
    
    func ftChatMessageAccessoryViewDidTappedOnItemAtIndex(_ index: NSInteger) {
        
        if index == 0 {
            
            let imagePicker : UIImagePickerController = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: { 
                
            })
        }else{
            let string = "I just tapped at accessory view at index : \(index)"
            
            print(string)
            
            //        FTIndicator.showInfo(withMessage: string)
            
            let message2 = FTChatMessageModel(data: string, time: "4.12 21:09:51", from: sender2, type: .text)
            
            self.addNewMessage(message2)
        }
    }
    
    //MARK: - FTChatMessageRecorderViewDelegate
    
    func ft_chatMessageRecordViewDidStartRecording(){
        print("Start recording...")
        FTIndicator.showProgressWithmessage("Recording...")
    }
    func ft_chatMessageRecordViewDidCancelRecording(){
        print("Recording canceled.")
        FTIndicator.dismissProgress()
    }
    func ft_chatMessageRecordViewDidStopRecording(_ duriation: TimeInterval, file: Data?){
        print("Recording ended!")
        FTIndicator.showSuccess(withMessage: "Record done.")
        
        let message2 = FTChatMessageModel(data: "", time: "4.12 21:09:51", from: sender2, type: .audio)

        self.addNewMessage(message2)
        
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        picker.dismiss(animated: true) {
            
            let image : UIImage = info[UIImagePickerControllerOriginalImage] as! UIImage
            let message2 = FTChatMessageImageModel(data: "", time: "4.12 21:09:51", from: self.sender2, type: .image)
            message2.image = image;
            self.addNewMessage(message2)
        }
    }
    
    func saveImageToDisk(image: UIImage) -> String {
        
        
        return ""
    }
    
    
    // MARK: Navigate to trip button
    
    
    func navigateToTripPlan() {
        let content = UNMutableNotificationContent()
        
        content.title = "Landmark Nearby"
        content.body = "Your only 15m away from Lake Wakatipu!"
        content.sound = UNNotificationSound.default()
        
        // Deliver the notification in five seconds.
        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 5, repeats: false)
        let request = UNNotificationRequest.init(identifier: "FiveSecond", content: content, trigger: trigger)
        
        // Schedule the notification.
        let center = UNUserNotificationCenter.current()
        center.add(request) { (error) in
            print(error)
        }
        print("should have been added")
        
        
//        sendMaiMessage1()
//
//        DispatchQueue.main.async {
//            self.performSegue(withIdentifier: "TripPlan", sender: self)
//        }
    }
    
    
    // MARK: Trigger messages for mai's convo
    
    
    let trigger1 = "Yes please"
    let trigger2 = "17/07/2017"
    
    override func ft_chatMessageInputViewShouldDoneWithText(_ textString: String) {
        makeTouristMessage(textString)
        
        switch textString {
        case trigger1:
            sendMaiMessage1()
            break
        case trigger2:
            // SendMessage2
            break
        default:
            break
        }
    }
    
    
    // MARK: Message sending functions
    
    
    func makeMaiMessage(_ text: String) {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/M hh:mm:ss"
        
        self.addNewMessage(FTChatMessageModel(data: text, time: formatter.string(from: Date()), from: sender1, type: .text))
    }
    
    func makeMaiMessageWithImage(_ imageUrl: String) {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/M hh:mm:ss"
        
        let imageMessage = FTChatMessageImageModel(data: imageUrl, time: formatter.string(from: Date()), from: sender1, type: .image)
        imageMessage.imageUrl = imageUrl
        
        self.addNewMessage(imageMessage)
    }
    
    func makeTouristMessage(_ text: String) {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/M hh:mm:ss"
        
        self.addNewMessage(FTChatMessageModel(data: text, time: formatter.string(from: Date()), from: sender2, type: .text))
    }
    
    
    // MARK: Chat script
    
    
    func sendMaiMessage1() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
            self.makeMaiMessage("The largest known 'longfin eel' was caught in the lake in 1886. Take a look!")
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                self.makeMaiMessageWithImage("http://4.bp.blogspot.com/-ElINPIUmhZM/U_xQHA19cMI/AAAAAAAAX9Y/ZO5JRVNsVoM/s1600/EEL%2BLONGFIN%2B(Anguilla%2Bdieffenbachii)%2BLong%2BFinned%2BNew%2BZealand%2B%2Bworld%2Brecord%2Bbiggest%2Bfish%2Bever%2Bcaught%2Bbig%2Bhuge%2Bfishes%2Brecords%2Blargest%2Bmonster%2Bfishing%2Bgiant%2Bsize%2Bimages%2Bpictures%2BIGFA%2Blb%2Bpound%2Brivers.jpg")
            }
        }
    }
    
    
    // MARK: Dismiss Keyboard
    
    
    func addDismissKeyboardTapGestureRecogniser() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ChatTableViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
}
