//
//  ChatTableViewCell.swift
//  Mai
//
//  Created by Eddie Chae on 29/07/17.
//  Copyright © 2017 Mai. All rights reserved.
//

import UIKit

class ChatRoomTableViewCell: UITableViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }


}

