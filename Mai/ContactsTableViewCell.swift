//
//  ContactsTableViewCell.swift
//  Mai
//
//  Created by Eddie Chae on 29/07/17.
//  Copyright © 2017 Mai. All rights reserved.
//

import UIKit

class ContactsTableViewCell: UITableViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!

//    open func setupWithUser(user : NIMUser) {
//        if let iconUrl : String = user.userInfo?.avatarUrl {
//            self.iconImageView.kf.setImage(with: URL(string: iconUrl)!)
//        }
//        self.nameLabel.text = user.userInfo?.nickName
//    }
    
}
